import qi
import sys


app = qi.Application(sys.argv)
app.start()
session = app.session

fts = session.service("FakeTabletService")

# Wait a customer and get its picture (in student backend)
print fts.wait_for_new_human() # print user ID

# Select product to display on robots tablet
    # 1: "casquette_femme"
    # 2: "casquette_homme"
    # 3: "parapluie_rose"
    # 4: "parapluie_noir"
    # 5: "sun_glasses_woman"
    # 6: "sun_glasses_man"
print fts.select_product(4) # print success status (True/False)

# Rank the face of the current human (in student backend)
print fts.rank_human_face() # Gives a grade (0-100 but can be a bit above 100)