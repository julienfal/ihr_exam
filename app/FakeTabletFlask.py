from flask import Flask, render_template
#import cv2
#import urllib
#import numpy as np
#import base64
from flask import request
import random

#from flask_bootstrap import Bootstrap5



app = Flask(__name__,
            static_url_path =   '', 
            template_folder =   'template',
            static_folder   =   'img')

#bootstrap = Bootstrap5(app)      

global human_index
global product_index

@app.route("/")
def home():

    global human_index

    try:
        human_index
    except NameError:
        human_index = "" #random.randint(1, 255)
        pass

    return render_template("index.html", human_index = human_index)

@app.route("/current_human")
def current_human():
    global human_index

    try:
        human_index
    except NameError:
        return ""
    else:
        return '<p><img src="https://boredhumans.b-cdn.net/faces2/' + str(human_index) + '.jpg" alt="Qries" width="400" height="400"/></p>'

    


@app.route("/new_human")
def new_human():
    global human_index
    human_index = random.randint(1, 255)
    return str(human_index)
    #return render_template('tablet.html', suggestions="<h1>TOTO</h1>")    


@app.route("/current_product")
def current_product():
    global product_index

    image_name =    {   1: "casquette_femme",
                        2: "casquette_homme",
                        3: "parapluie_rose",
                        4: "parapluie_noir",
                        5: "sun_glasses_woman",
                        6: "sun_glasses_man"
                    }

    try:
        product_index
    except NameError:
        return ""
    else:
        product_index = int(product_index)
        if product_index in range(1,7):
            return '<p><img src="' + image_name[product_index] + '.jpg" alt="Qries" width="400" height="400"/></p>'


@app.route("/new_product")
def new_product():
    global product_index
    product_index = request.args.get('id')
    return product_index 


if __name__ == "__main__":
    app.run(host ='0.0.0.0', port = 5001, debug = True) 