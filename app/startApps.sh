#!/bin/bash
# install Naoqi Pepper environement

set -e

# Naoqi python
export PYTHONPATH=$PYTHONPATH:/app/softbankRobotics/pynaoqi-python2.7-2.5.5.5-linux64/lib/python2.7/site-packages

# Choregraphe
export PATH=$PATH:/app/softbankRobotics/choregraphe-suite-2.5.10.7-linux64/bin

echo "------------------------"
echo $PYTHONPATH
echo "------------------------"
naoqi-bin &
#P1=$!
sleep 10

python FakeTabletFlask.py &
#P2=$!
python FakeTabletApp.py 
#P3=$!
#wait $P1 $P2 $P3
