#! /usr/bin/env python
# -*- encoding: UTF-8 -*-

"""Example: A Simple class to test dialogue and media"""

import qi
import time
import sys
import argparse
#import urllib
import requests
import random
import time
import zerorpc
import socket


class FakeTabletService(object):
    """
    A simple class to react to face detection events.
    """

    def __init__(self, app):
        """
        Initialisation of qi framework and event detection.
        """

        # Get the service ALMemory.
        self.memory = app.session.service("ALMemory")

        self.human_id = None


    def wait_for_new_human(self):

        wait_human = random.randint(1, 5)

        time.sleep(wait_human)
        self.human_id = requests.get(url = "http://127.0.0.1:5001/new_human").text

        #requests.get('http://127.0.0.1:5001/new_human') as response:
        #    print response
        #    self.human_id = response.read()
        
        return self.human_id


    def select_product(self, p):

        if int(p) in range(1,6):
            response = requests.get('http://127.0.0.1:5001/new_product?id='+ str(p)) 
            return True
        
        return False


    def rank_human_face(self):
        if self.human_id == None:
            return -1

        else:
            s = zerorpc.Client(heartbeat=None, timeout=4)



            url = 'https://boredhumans.b-cdn.net/faces2/' + self.human_id + '.jpg'
            r = requests.get(url, allow_redirects=True)
            open('face.jpg', 'wb').write(r.content)

            try:
                # Connection à un Service FaceRank qui donne un score à partir d'un visage 
                s.connect("tcp://vps.cpe-sn.fr:54327")
                data_path = "face.jpg"
                data = open(data_path, "rb").read()
                img, score = s.face_score(data)
                dat = img
            except:
                print("FaceRank failed !!! Gives a random grade ! Sorry !")
                score = random.randint(60, 100)
            

            return score
