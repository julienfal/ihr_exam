import qi
import sys
from FakeTabletService import FakeTabletService  

#create an application
app = qi.Application(sys.argv)
app.start()

#create an instance of MyFooService
fts = FakeTabletService(app)

session = app.session
#let's register our service 
id = session.registerService("FakeTabletService", fts)

#let the application run
app.run()