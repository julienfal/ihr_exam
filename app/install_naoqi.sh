#!/bin/bash
# install Naoqi Pepper environement

set -e

NAOQI_PEPPER_INSTALL_FOLDER=softbankRobotics

#cd
mkdir $NAOQI_PEPPER_INSTALL_FOLDER
cd $NAOQI_PEPPER_INSTALL_FOLDER


# Naoqi python
echo -e "\e[32m--- Download & Unzip pyNaoqi ---\e[0m"
WGET_ARCHIVE_NAME=pynaoqi-python2.7-2.5.5.5-linux64.zip
WGET_ARCHIVE_GDRIVE_ID=192DPewC4HxZgV8J-0p-3pARs-fiL1iS1
if test ! -f "$WGET_ARCHIVE_NAME"; then
    wget --load-cookies /tmp/cookies.txt "https://docs.google.com/uc?export=download&confirm=$(wget --quiet --save-cookies /tmp/cookies.txt --keep-session-cookies --no-check-certificate 'https://docs.google.com/uc?export=download&id='${WGET_ARCHIVE_GDRIVE_ID}'' -O- | sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/\1\n/p')&id=${WGET_ARCHIVE_GDRIVE_ID}" -O $WGET_ARCHIVE_NAME && rm -rf /tmp/cookies.txt 
    #read -p "Press any key..."
    unzip ${WGET_ARCHIVE_NAME} -d .
fi
echo -e "\e[32m- Set PYTHONPATH in .bashrc : \e[0m"
export PYTHONPATH=${PYTHONPATH}:"/app/softbankRobotics/pynaoqi-python2.7-2.5.5.5-linux64/lib/python2.7/site-packages"
echo " "


# Choregraphe
# echo -e "\e[32m--- Download & Unzip choregraphe ---\e[0m"
# WGET_ARCHIVE_NAME=choregraphe-suite-2.5.10.7-linux64.tar.gz
# WGET_ARCHIVE_GDRIVE_ID=1ECnX7PceLpJc66-90rX-rcuQCAERQMoc
# if test ! -f "$WGET_ARCHIVE_NAME"; then
#     wget --load-cookies /tmp/cookies.txt "https://docs.google.com/uc?export=download&confirm=$(wget --quiet --save-cookies /tmp/cookies.txt --keep-session-cookies --no-check-certificate 'https://docs.google.com/uc?export=download&id='${WGET_ARCHIVE_GDRIVE_ID}'' -O- | sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/\1\n/p')&id=${WGET_ARCHIVE_GDRIVE_ID}" -O $WGET_ARCHIVE_NAME && rm -rf /tmp/cookies.txt 
#     #read -p "Press any key..."
#     tar xzvf ${WGET_ARCHIVE_NAME}
# fi
# echo -e "\e[32m- Set PATH in .bashrc : \e[0m"
# export PATH=$PATH:"/app/softbankRobotics/choregraphe-suite-2.5.10.7-linux64/bin"
# echo " "
