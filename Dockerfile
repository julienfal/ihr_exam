FROM python:2.7.18-slim
#FROM ubuntu:14.04
COPY app /app
WORKDIR /app


#RUN apt-get update && apt-get install -y python-pip && python -m pip install --upgrade pip
RUN apt-get update && apt-get install -y python-pip iproute2 telnet iputils-ping
#RUN python -m pip install --upgrade pip
RUN python -m pip install -r requirements.txt
EXPOSE 5001 
EXPOSE 9559
#ENTRYPOINT [ "python" ]

#RUN ./install_naoqi.sh

#ADD boost/* /app/softbankRobotics/pynaoqi-python2.7-2.5.5.5-linux64/

# Set the path to the SDK
#ENV PYTHONPATH=${PYTHONPATH}:/naoqi/pynaoqi-python2.7-2.5.5.5-linux64/
#ENV LD_LIBRARY_PATH="/naoqi/pynaoqi-python2.7-2.5.5.5-linux64:$LD_LIBRARY_PATH"

#RUN bash -c 'echo -e $PYTHONPATH'

#CMD [ "export", 'PYTHONPATH=${PYTHONPATH}:"/app/softbankRobotics/pynaoqi-python2.7-2.5.5.5-linux64/lib/python2.7/site-packages"']

#RUN bash -c 'echo -e $PYTHONPATH'

#CMD ["/bin/bash", "startApps.sh"]

#CMD [ "naoqi-bin" ]
CMD [ "python", "FakeTabletFlask.py"]
#CMD [ "python", "FakeTabletApp.py" ]