# Examen Robotique - S8 - IHR - 2022
## Avant de lire le sujet...
- Sur e-Campus:
  - Suivez dans la partie TP les informations d'installation de l'environnement (bashrc) en salle TP (déjà fait pour tous ceux qui ont utilisés leur session en TP)
  - Récupérez ce repo git. Vous en executerez le contenu pendant l'examen mais vous ne le modifierez en aucun cas sauf mention contraire.
- Dans un terminal, lancez le fichier run.sh (à la racine de l'archive) --> pendant son execution (1ère execution longue), lisez **TOUT** l'énoncé
- Si certains aspects du sujet vous choque, c'est justement pour que vous puissiez en discuter sur les questions d'éthique.


## Consigne de rendu
- Votre rendu se fait depuis un repo Gitlab **créé par le prof**  
- Les justifications / explications / réponses aux questions se font dans un fichier README.md à la racine du projet, et numérotées dans le même ordre que les questions. Chaque question fera l'objet d'un titre avec `##` (e.g. `## 4. Questions de cour`)
- Pénalités pour les commits/push en retard

## Scénario du sujet

Faites un dialogue entre un robot et un humain, au travers d'un terminal ou de Choregraphe (mais pas de developpement sous Choregraphe)

Ce dialogue doit être entreprenant, riche, animé, illustré.

Le "robot" doit interpeller des passants à la sortie d'un cinéma. Dans l'échange qu'il a avec eux, il doit se débrouiller pour avoir si possible :
- le prénom de la personne
- le code postale de la personne 
- la photo de la personne (voir explication dans l'aide)

A partir du code postal, essayez de déterminer la météo au domicile de la personne (webservices). A partir de la météo, proposez à la vente soit un parapluie, soit une casquette. A partir du prénom (webservice), proposez soit un modèle "féminin", soit un modèle "masculin".


Dans la mesure où vous avez pu faire ce qui précède :  
  -  si la distance entre le robot (IP) et le domicile de la personne est supérieur à 6km, proposez (pas d'image prévue) un Uber, sinon une trotinette électrique (2 points)
  -  si la personne est "belle", proposez lui plutôt des lunettes de soleil en alternative à la casquette (0.5 points)
  -  si l'échange ne permet pas d'avoir le code postal, prenez la position courante du robot. (0.5 points)
  -  si l'échange ne permet pas d'avoir le prénom, proposez les articles masculins (0.5 points)



## Questions (note sur 20)

### 1. Intro - 1 POINT
Copiez-collez le contenu du fichier `run.sh` dans votre README, entre des balises ```bash (cf ci-dessous, voir fichier .md) et ajoutez quelques lignes de commentaires qui permettent de comprendre ce qui est lancé.  
ex :
```bash
 # blablabla
 naoqi-bin &
```


### 2. Scénario - 12 POINTS :
Réalisez l'ensemble du scénario décrit ci-dessus. Si un élément vous bloque ou si vous manquez de temps, choisissez de simplifier le scénario **en expliquant vos choix**. Il faudra forcement faire des choix de simplification. Par exemple il semble compliqué de gérer les interactions de paiements. Ce n'est d'ailleurs pas demandé, mais tout est question de choix dans l'interaction, et ces choix sont à expliquer.


Le projet prend en compte les bonnes pratiques d'interaction d'un robot (dans la mesure du possible). La gestuelle du robot peut être observée sous Choregraphe, en se connectant sur le robot virtual (ip local). Par vos explications, guidez le correcteur dans le lancement et l'usage de votre programme. Mieux vaut un programme incomplet qui fonctionne, qu'un programme en chantier qui ne fonctionne pas. Je propose de pousser sur la branche 'master' dès que cela marche, et d'être sur une branche 'dev' tant qu'une fonctionnalité n'est pas opérationnelle.

### 3. Ethique - 4 POINTS : 
A partir des documents de la CNIL et du COMEST, discutez des problématiques éthiques qui peuvent être soulevées par le fonctionnement proposé par le robot. Les points soulevés qui ne font pas référence aux documents cités ne seront que peu considérés.


### 4. Questions de cours - 3 POINTS :
  - 4.1. (1 point) Quelle est la différence entre le bio-mimétisme anatomique et physiologique ? Donnez des exemples
  - 4.2. (0,5 point) Expliquez avec vos mots, la "vallée de l'étrange"
  - 4.3. (1,5 point) Concernant les prothèses/augmentations technologiques, donnez 2 problematiques pour chacun des critères suivants :
    - Durée de vie de l'appareil
    - Risques vitaux
    - Accessibilité
    - Appropriation

## Indications complémentaires / Aide

### Environnement de travail
Répondre à la question 1 est primordial pour bien cerner la question 2. 
Après avoir analysé et lancé l'environnement robotique simulé avec `run.sh` :
- Ouvrez un navigateur web et entrez l'adresse `http://127.0.0.1:5001/` --> Une page avec pour titre `Interface examen IHR 2022` doit s'afficher, avec à gauche, des infos pour le programmateur, et à droite une "fausse tablette" pour la personne en interaction avec le robot.
-  lancez le code `FakeTabletTest.py` présent dans le dossier `app` après avoir avoir sourcé le `venv`. Observez le code et le fonctionnement. Vous pouvez lancer le code plusieurs fois en le modifiant pour bien le comprendre. Vous utiliserez ces méthodes dans votre propre code.  

:warning: Certains services tel que ALTabletService, n'existent pas simulés sur l'ordinateur


#### Photos des personnes
La méthode `wait_for_new_human` (cf code de test), attend un temps aléatoire (court) puis prend une photo de la personne en interaction avec le robot. Dans les faits, ce sont des "faux" visages générés par une IA. Vous pourrez d'ailleurs voir un certain nombre d'artéfacts sur les images. Ce n'est donc pas votre photo, mais c'est vous qui interagissez avec le robot.

#### La fausse tablette
Le seul changement que vous pouvez effectuer sur la fausse tablette (partie droite de la page web), se fait avec la methode `select_product` du service `FakeTabletService` (cf code de test). Je rapelle que le Service `ALTabletService` n'est pas disponible !

### Choregraphe

#### Connexion au robot
Connectez-vous au naoqi lancé sur l'ordinateur en spécifiant l'IP (127.0.0.1) et le port (9559) à droite de la fenêtre de connexion.

#### Visualiser mouvements du robot
Une fois connecté vous voyez le robot sur la droite. Vous voyez ses mouvements et vous pouvez les quantifier ou éventuellement les modifier en cliquant sur les membres du robot.

#### Afficher/modifier variables de l'ALMemory
Menu `View` --> `Memory Watcher`

#### Afficher l'interface textuelle de dialogue 
Menu `View` --> `Dialog`



### Ressources

#### Web services
- Metadonnées localisation : https://apipheny.io/free-api/#apis-without-key
- Météo : https://api.openweathermap.org/data/2.5/weather?lat=45.78412939935352&lon=4.869497007956656&APPID=49b584e311c58fa09794e5e25a19d1af&UNITS=metric"

#### Doc Naoqi

- [Syntaxe *.top](http://doc.aldebaran.com/2-5/naoqi/interaction/dialog/dialog-syntax_full.html)
- [ALMemory](http://doc.aldebaran.com/2-5/naoqi/core/almemory-api.html)