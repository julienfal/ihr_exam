
set -e

python2 -m virtualenv venv 
source venv/bin/activate
cd app
python -m pip install -r requirements.txt

naoqi-bin &
p1=$!

sleep 1

python FakeTabletFlask.py &
p2=$!

python FakeTabletApp.py 
p3=$!


# Kill
deactivate
kill -9 $p1
kill -9 $p2
pkill -9 -f "venv/bin"
